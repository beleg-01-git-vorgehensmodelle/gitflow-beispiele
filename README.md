# Gitflow-Beispiele

Dieses Repository beinhaltet die Gitflow-Beispiele für den Beleg "Best Practices für Git-Vorgehensmodelle in größeren Teams".
Die Beispiele, die in sich selbst abgeschlossene Repositorys sind, befinden sich in den entsprechenden ZIP-Archiven.
Um ein Repository zu verwenden, gilt es lediglich das entsprechende ZIP-Archiv herunterzuladen (oder alle über Klonen des Repositorys) und das Beispielrepository aus dem ZIP-Archiv zu extrahieren.
ZIP-Archive, die mit "-Start" beginnen, sind die Starterbeispiele, ZIP-Archive, die mit "-Fertig" enden, die fertiggestellten Beispiele (ausgehend von den Start-Beispielen).